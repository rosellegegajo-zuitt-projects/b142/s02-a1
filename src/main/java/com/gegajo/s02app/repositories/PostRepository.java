package com.gegajo.s02app.repositories;

import com.gegajo.s02app.models.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Object> {
}
